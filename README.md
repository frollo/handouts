# Handouts
## Fantasy RPG handouts in LaTeX

Handouts is a package to typeset medieval looking PDFs using LaTeX. It uses handwritten fonts, a nice parchment background and offers some environments.

This package is not for advanced typesetting. It's a simple layout which should help to produce one or two pages of text which are still readable while having a medieval look. When using the package the default font will be set to Lukas Svatba. In order to keep the document readable, the font size will be increased to 120%. At the moment nothing but text is managed.

## Usage

At the moment handouts offers a small set of options:
  * `bgimage` add a parchment background
  * `smallmargin` sets all the margins to 0.5 inches
  * `mediummargin` sets all the margins to 0.7 inches

You can use both `smallmargin` and `mediummargin` in the same document. One of them will not work. If neither of them is used, the margin will not be modified.

### Environments

There are two environments:
  * `header` which, at the moment, is simply a wrapper for `flushright`
  * `signature` which uses `flushright` and sets the font to Auriocus Kalligraphicus

### Commands
`red` is a wrapper for `\textcolor{red}`

## TODO

  * Wrap `smallmargin` and `mediummargin` in a single key-value option
  * Use key-value options to offer more background images
  * Select a color for `red` which looks better on parchment
  * Add more refined (and complete) options for titles

## Acknowledgments

Ysingrinus had the original idea and gave me useful tips and suggestions during development

The background image comes from [MyFreeTextures](http://www.myfreetextures.com/free-old-brown-vintage-parchment-paper-texture/)

The used fonts are all in the `aurical` package
